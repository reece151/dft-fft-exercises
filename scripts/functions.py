''' functions.py
Common functions for I/Q signal processing
Reece Kibble, 2018
'''

import numpy as np
import scipy.signal as sig
import wave
import struct


def getFFT(signal, rangeI, rangeF, Fs, Fc):
    '''
    Gets the fast fourier transform of the given signal within the rangeI -> rangeF.
    Uses the sample frequency and center frequency to give a spectrum in Hz.
    Returns the calculated frequency range and the normalized FFT transform
    '''    
    # get desired segment of signal
    signalSeg = signal[rangeI:rangeF]

    # get shifted and normalised FFT of signal
    signalSegFFT = np.fft.fftshift(np.fft.fft(signalSeg))
    signalSegFFTAbs = abs(signalSegFFT)
    signalSegFFTNorm = 20*np.log10(signalSegFFTAbs/(np.max(signalSegFFTAbs))) 

    # get range of frequencies
    signalSegFFTNormLength = len(signalSegFFTNorm)
    freqMin = Fc-Fs/2 #minimum frequency
    freqRange = list(range(0,signalSegFFTNormLength,1))
    for n in freqRange:
        freqRange[n] = n*Fs/signalSegFFTNormLength + freqMin

    return signalSegFFTNorm, freqRange


def demodulateFM(signal, Fs):
    '''
    produce demodulated FM signal from input frequency domain signal
    '''
    Fbw = 0.2 #FM bandwidth ~200KHz
    decFactor = int(Fs/Fbw)
    
    #decimate by factor decFactor, ready for demodulation
    signalDec = sig.decimate(signal, decFactor, ftype='fir')
    signalDecFs = Fs/decFactor

    # get normalised signal
    signalDecNorm = signalDec/abs(signalDec)
    
    # demodulate using polar discriminator method
    signalDemod = signalDecNorm[1:]*np.conj(signalDecNorm[:-1])
    signalDemod = np.angle(signalDemod)

    return signalDemod, signalDecFs


def toAudio(signal, Fs, audioFs, volume, fileName):
    '''
    produce audio file from the demodulated FM signal
    '''

    #decimate from Fs to audioFs
    decFactor =  int(Fs/audioFs)
    signalDec = sig.decimate(signal, decFactor, ftype='fir')
    # get normalised signal
    signalAudio = signalDec/abs(signalDec)

    # wav audio file output
    print('generating audio file...')
    wav_file = wave.open('output/' + fileName, 'w')
    wav_file.setnchannels(1)
    wav_file.setsampwidth(2)
    wav_file.setframerate(audioFs)
    wav_file.setnframes(len(signalAudio))
    wav_file.setcomptype("NONE", "no compression")

    for s in signalAudio:
        wav_file.writeframes(struct.pack('h', int(s*volume))) #applies volume scalar to each signal during write process
    print('done!')

    return


