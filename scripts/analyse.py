''' analyse.py
Loads, decodes, demodulates and plots the data in multiple forms. Assumes Data is available and in finite size.
Attempts to output demodulated data as a playable output.wav audio file.
Uses functions from functions.py
Reece Kibble, 2018
'''

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import numpy as np
import matplotlib.pyplot as plt

import functions as fn


# get raw data from file, as array of unsigned 8-bit integers
dataRaw = np.fromfile('data/Perth_FM_99.3MHz_2Ms_rtl.dat', dtype = np.dtype('uint8'))
dataFc = 99.3 #set center frequency to 99.3 MHz
dataFs = 2 #set sampling frequency to 2Ms (= 2 MHz)

# transform raw data into complex I/Q data
# raw data given as interleaved real and imaginary numbers. subtraction of 128 to get actual value because offset 128 = 0v
dataComplex = (dataRaw[0::2]-np.float32(128)) + 1j*(dataRaw[1::2]-np.float32(128))

dataLength = len(dataComplex)
print("Total samples: ", dataLength, " Total length(s): ", dataLength/(dataFs*10**6))

# plotthe first 250 entries of real (I) and imaginary (Q) values 
plt.figure(1)
plt.title('I/Q Signal (250 samples)')
plt.ylabel('Voltage')
plt.xlabel('Samples')
plt.plot(dataComplex[0:250].real)
plt.plot(dataComplex[0:250].imag)
plt.savefig('output/IQSignal.jpg')


# FFT to get frequency response spectrum of first 10000 samples
# Assuming data sample rate of 2 Ms and a center frequency of 99.3 MHz
dataFFT, dataFFTFreq = fn.getFFT(dataComplex, 0, 10000, dataFs, dataFc)

# Plot the spectrum:
plt.figure(2)
plt.title('FFT of I/Q: Spectrum around 99.3 MHz')
plt.ylabel('Relative amplitude (dB)')
plt.xlabel('Frequency (MHz)')
plt.plot(dataFFTFreq, dataFFT)
# Identified three radio stations within spectrum
plt.text(98.5, 0, '98.5FM: Sonshine', horizontalalignment='center')
plt.text(99.3, 0, '99.3FM: Triple J', horizontalalignment='center')
plt.text(100.1, 0, '100.1FM: Curtin', horizontalalignment='center')
plt.savefig('output/FFTSpectrum.jpg')


# FM Demodulate
dataDemod, dataDemodFs = fn.demodulateFM(dataComplex, dataFs)
# NOTE: dataDemod signal now real only

# Plot PSD of demodulated signal
plt.figure(3)
plt.psd(dataDemod, NFFT=2048, Fs=dataDemodFs*10**6)
plt.title("Demodulated Power Spectral Density")
# Show the typical spectrum of composite baseband signal for comparison (https://en.wikipedia.org/wiki/FM_broadcasting)
plt.axvspan(0,             15000,         color="red", alpha=0.2)  
plt.axvspan(19000-500,     19000+500,     color="green", alpha=0.2)  
plt.axvspan(19000*2-15000, 19000*2+15000, color="orange", alpha=0.2)
plt.axvspan(19000*3-1500,  19000*3+1500,  color="blue", alpha=0.2)  
plt.text(15000/2, -80, 'Mono', horizontalalignment='center')
plt.text(19000*2, -80, 'Stereo L-R', horizontalalignment='center') 
plt.savefig('output/demodulatedPSD.jpg') 

# Output audio
fn.toAudio(dataDemod, dataDemodFs*(10**6), 17*(10**3), 5000, 'output.wav')


plt.show()


