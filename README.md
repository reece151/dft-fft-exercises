# DFT FFT Exercises

Exercises for handling of digital data and signal processing using Digital Fourier Transforms (DFT) via Fast Fourier Transforms (FFT). Reads data and outputs an I/Q signal sample, FFT spectrum, and a demodulated Power Spectral Density (PSD). Also attempts to output demodulated data as a playable output.wav audio file.

Runs on Python 3.6.7 within Ubuntu 18.04 LTS

## Dependancies
python3: numpy, scipy.signal and matlabplotlib.pyplot

## Usage
Run analyse.py in python3 from the root directory:

```
python3 .../dft-fft-exercises/scripts/analyse.py
```
Outputs (plots and audio file) are created in 'output' directory